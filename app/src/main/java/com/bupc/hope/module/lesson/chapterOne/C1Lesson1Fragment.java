package com.bupc.hope.module.lesson.chapterOne;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.bupc.hope.R;
import com.bupc.hope.core.adapter.chapter.ImageAdapter2;
import com.bupc.hope.core.adapter.chapter.ImageAdapter3;
import com.bupc.hope.core.model.Image;
import com.bupc.hope.core.utils.HtmlUtilities;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class C1Lesson1Fragment extends Fragment {

    RecyclerView rvC1Lesson1;
    ImageAdapter3 imageAdapter2;

    WebView wvC1Lesson1Intro1;
    WebView wvC1Lesson1Intro2;

    public C1Lesson1Fragment() {
        // Required empty public constructor
    }

    public static C1Lesson1Fragment newInstance() {

        Bundle args = new Bundle();

        C1Lesson1Fragment fragment = new C1Lesson1Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageAdapter2 = new ImageAdapter3(getActivity(), getImageResource());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_c1_lesson1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        String content = getString(R.string.c1_lesson1_intro1);
        String content2 = getString(R.string.c1_lesson1_intro2);

        wvC1Lesson1Intro1 = (WebView) view.findViewById(R.id.wvC1Lesson1Intro1);
        wvC1Lesson1Intro1.loadData(HtmlUtilities.toHtmlFormat(content),
                HtmlUtilities.CONTENT_TYPE,
                null);

        wvC1Lesson1Intro2 = (WebView) view.findViewById(R.id.wvC1Lesson1Intro2);
        wvC1Lesson1Intro2.loadData(HtmlUtilities.toHtmlFormat(content2),
                HtmlUtilities.CONTENT_TYPE,
                null);

        rvC1Lesson1 = (RecyclerView) view.findViewById(R.id.rvC1Lesson1);
        rvC1Lesson1.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvC1Lesson1.setAdapter(imageAdapter2);
    }

    public List<Image> getImageResource() {
        List<Image> images = new ArrayList<>();

        String[] descriptions = getActivity().getResources().getStringArray(R.array.c1_lesson1_content_list);
        int imageSource[] = {
                R.drawable.c1_running,
                R.drawable.c1_weightlift,
                R.drawable.c1_pushups,
                R.drawable.c1_trunkforward,
                R.drawable.c1_waist
        };

        int drawable = 0;
        for (String description : descriptions) {
            images.add(new Image.Builder()
                        .description(description)
                        .src(imageSource[drawable])
                        .build());
            drawable++;
        }

        return images;
    }
}
