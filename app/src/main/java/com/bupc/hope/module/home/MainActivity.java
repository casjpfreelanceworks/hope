package com.bupc.hope.module.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.bupc.hope.R;
import com.bupc.hope.core.enumz.Page;
import com.bupc.hope.core.model.Chapter;
import com.bupc.hope.core.utils.HopeDataUtilities;
import com.bupc.hope.module.about.AboutFragment;
import com.bupc.hope.module.chapter.ChapterActivity;
import com.bupc.hope.module.curriculum.CurriculumFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    List<Chapter> chapters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        chapters = new ArrayList<>();
        chapters = HopeDataUtilities.getResourceData(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // set the initial content to home
        setupContent(Page.HOME);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        onNavigationClick(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // select which item was click on navigation view
    private void onNavigationClick(int navMenuClick) {
        switch (navMenuClick) {
            case R.id.nav_home:
                setupContent(Page.HOME);
                break;
            case R.id.nav_deped_curriculum:
                setupContent(Page.CURRICULUM);
                break;
            case R.id.nav_about:
                setupContent(Page.ABOUT);
                break;
            case R.id.chapter_one:
                startChapterActivity(0);
                break;
            case R.id.chapter_two:
                startChapterActivity(1);
                break;
            case R.id.chapter_three:
                startChapterActivity(2);
                break;
            case R.id.chapter_four:
                startChapterActivity(3);
                break;
            default :
                break;
        }
    }

    // start chapter activity
    public void startChapterActivity(int chapter) {
        Intent intent = new Intent(this, ChapterActivity.class);
        intent.putExtra(ChapterActivity.EXTRA_TITLE, chapters.get(chapter));
        startActivity(intent);
    }

    private void setupContent(Page page) {

        Fragment fragment = null;

        // choice between the fragment
        switch (page) {
            default:
            case HOME: // set home as default fragment
                fragment = HomeFragment.newInstance();
                break;
            case CURRICULUM: // for curruculum view
                fragment = CurriculumFragment.newInstance();
                break;
            case ABOUT: // for about view
                fragment = AboutFragment.newInstance();
                break;
        }

       // setup the fragmnet
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_main, fragment)
                .commit();

    }
}
