package com.bupc.hope.module.lesson.chapterThree;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.bupc.hope.R;
import com.bupc.hope.core.adapter.chapter.ImageAdapter2;
import com.bupc.hope.core.model.Image;
import com.bupc.hope.core.utils.HtmlUtilities;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class C3LessonFragment extends Fragment {

    private static final String CONTENT_INDEX = "content";

    private TextView tvC3Title;
    private WebView tvC3Content;

    private String content[] = new String[10];
    private String title[] = new String[10];

    RecyclerView rvImages;

    int contentIndex = 0;

    ImageAdapter2 imageAdapter2;
    View cvImageContainer;

    public C3LessonFragment() {
        // Required empty public constructor
    }


    public static C3LessonFragment newInstance(int contentIndex) {

        Bundle args = new Bundle();
        args.putInt(CONTENT_INDEX, contentIndex);

        C3LessonFragment fragment = new C3LessonFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = getActivity().getResources().getStringArray(R.array.c3_lesson_contents);
        title = getActivity().getResources().getStringArray(R.array.hope3_lesson);
        contentIndex = getArguments().getInt(CONTENT_INDEX, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_c3_lesson, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvC3Title = (TextView) view.findViewById(R.id.tvC3Title);
        tvC3Content = (WebView) view.findViewById(R.id.tvC3Content);

        rvImages = (RecyclerView) view.findViewById(R.id.rvImages);
        cvImageContainer = view.findViewById(R.id.cvImageContainer);

        if (contentIndex >= title.length && contentIndex >= content.length) {
            contentIndex = 0;
        }

        if (contentIndex != 0 && contentIndex != 3) {
            imageAdapter2 = new ImageAdapter2(getActivity(), getImageAdapter(contentIndex));
            rvImages.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvImages.setAdapter(imageAdapter2);
        } else {
            cvImageContainer.setVisibility(View.INVISIBLE);
        }

        WebSettings webSettings = tvC3Content.getSettings();
        webSettings.setDefaultTextEncodingName(HtmlUtilities.ENCODING);

        tvC3Title.setText(title[contentIndex]);
        tvC3Content.loadData(HtmlUtilities.toHtmlFormat(content[contentIndex]),
                HtmlUtilities.CONTENT_TYPE,
                HtmlUtilities.ENCODING);


    }

    public List<Image> getCheerdanceDrawable() {
        List<Image> images = new ArrayList<>();
        int drawables[] = {R.drawable.cheerdance_bow_and_arrow1,
                R.drawable.cheerdance__heelstrech2,
                R.drawable.cheerdance_scorpion3,
                R.drawable.cheerdance_swedish_fall_4};
        String titles[] = {"Bow and Arrow", "Heelstrech", "Scorpion", "Swedish Fall"};

        for (int i = 0; i < 4; i++) {
            images.add(new Image.Builder()
                    .description(titles[i])
                    .src(drawables[i])
                    .build());
        }

        return images;
    }

    public List<Image> getContemporaryDrawable() {
        List<Image> images = new ArrayList<>();
        int drawables[] = {R.drawable.contemporary_plie,
                R.drawable.contemporary_releve};
        String titles[] = {"Plie (plee-ay)", "Releve (ruh-leh-vay)"};

        for (int i = 0; i < 2; i++) {
            images.add(new Image.Builder()
                    .description(titles[i])
                    .src(drawables[i])
                    .build());
        }

        return images;
    }

    public List<Image> getFestivalDrawable() {
        List<Image> images = new ArrayList<>();
        int drawables[] = {R.drawable.festival_aliwan,
                R.drawable.festival_ati_atihan,
                R.drawable.festival_dinagyang,
                R.drawable.festival_kadayawan,
                R.drawable.festival_mass_kara,
                R.drawable.festival_moriones,
                R.drawable.festival_pahiyas,
                R.drawable.festival_panagbenga,
                R.drawable.festival_pintados,
                R.drawable.festival_sinulog};
        String titles[] = {"Aliwan",
                "Ati-Atihan",
                "Dinagyang",
                "Kadayawan",
                "Mass Kara",
                "Morianos",
                "Pahiyas",
                "Panagbenga",
                "Pintados",
                "Sinulog"};


        for (int i = 0; i < 10; i++) {
            images.add(new Image.Builder()
                    .description(titles[i])
                    .src(drawables[i])
                    .build());
        }

        return images;
    }

    public List<Image> getFolkdancelDrawable() {
        List<Image> images = new ArrayList<>();
        int drawables[] = {R.drawable.folkdance_banga,
                R.drawable.folkdance_idaw,
                R.drawable.folkdance_idudu, R.drawable.folkdance_salakot, R.drawable.folkdance_tinikling};
        String titles[] = {"Banga", "Idaw", "Idudu", "Salakot", "Tinikling"};

        for (int i = 0; i < 5; i++) {
            images.add(new Image.Builder()
                    .description(titles[i])
                    .src(drawables[i])
                    .build());
        }

        return images;
    }

    public List<Image> getImageAdapter(int contentIndex) {
        List<Image> images = new ArrayList<>();
        switch (contentIndex) {
            default:
            case 1:
                images = getCheerdanceDrawable();
                break;
            case 2:
                images = getContemporaryDrawable();
                break;
            case 4:
                images = getFolkdancelDrawable();
                break;
            case 5:
                images = getFestivalDrawable();
                break;
        }

        return images;

    }


}
