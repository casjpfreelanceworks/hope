package com.bupc.hope.module.home;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bupc.hope.R;
import com.bupc.hope.core.adapter.ChapterAdapter;
import com.bupc.hope.core.model.Chapter;
import com.bupc.hope.core.utils.HopeDataUtilities;
import com.bupc.hope.module.chapter.ChapterActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements ChapterAdapter.OnItemClickListener {

    private ChapterAdapter chapterAdapter;
    private RecyclerView rvHope;

    private List<Chapter> chapters;


    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chapters = new ArrayList<>();
        chapters = HopeDataUtilities.getResourceData(getActivity());
        chapterAdapter = new ChapterAdapter(getActivity(), chapters, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvHope = (RecyclerView) view.findViewById(R.id.rvHope);
        rvHope.setLayoutManager(new LinearLayoutManager(getActivity()));

        rvHope.setAdapter(chapterAdapter);

    }

    @Override
    public void onItemClick(int position) {
        chapters.get(position);
        Intent intent = new Intent(getActivity(), ChapterActivity.class);
        intent.putExtra(ChapterActivity.EXTRA_TITLE, chapters.get(position));
        startActivity(intent);
    }
}
