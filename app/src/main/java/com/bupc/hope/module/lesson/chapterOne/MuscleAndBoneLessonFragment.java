package com.bupc.hope.module.lesson.chapterOne;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bupc.hope.R;
import com.bupc.hope.core.adapter.chapter.one.ImageAdapter;
import com.bupc.hope.core.model.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MuscleAndBoneLessonFragment extends Fragment {


    ImageAdapter imageAdapter;
    RecyclerView rvMuscleAndBoneStrengthenings;

    public static MuscleAndBoneLessonFragment newInstance() {

        Bundle args = new Bundle();

        MuscleAndBoneLessonFragment fragment = new MuscleAndBoneLessonFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public MuscleAndBoneLessonFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageAdapter = new ImageAdapter(getActivity(), getImages());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_muscle_and_bone_lesson, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvMuscleAndBoneStrengthenings = (RecyclerView) view.findViewById(R.id.rvMuscleAndBoneStrengthenings);
        rvMuscleAndBoneStrengthenings.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMuscleAndBoneStrengthenings.setAdapter(imageAdapter);
    }

    /**
     * Images
     * @return
     */
    private List<Image> getImages() {
        List<Image> images = new ArrayList<>();

        String titles[] = getActivity().getResources().getStringArray(R.array.chapter_one_lesson_two);
        int imageSource[] = { R.drawable.chapter1_lesson2_quadriceps,
                    R.drawable.chapter1_lesson2_hamstrings,
                    R.drawable.chapter1_lesson2_calf,
                    R.drawable.chapter1_lesson2_pectoral,
                    R.drawable.chapter1_lesson2_trapezius,
                    R.drawable.chapter1_lesson2_deltoid,
                    R.drawable.chapter1_lesson2_biceps,
                    R.drawable.chapter1_lesson2_triceps,
                    R.drawable.chapter1_lesson2_forearm,
                    R.drawable.chapter1_lesson2_abdominal,
                    R.drawable.chapter1_lesson2_erector_spinae };
        int drawableIndex = 0;
        for (String title : titles) {
            Image.Builder builder = new Image.Builder()
                    .title(title)
                    .src(imageSource[drawableIndex]);
            images.add(builder.build());
            drawableIndex++;
        }

        return images;
    }

}
