package com.bupc.hope.module.lesson.chapterTwo;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.bupc.hope.R;
import com.bupc.hope.core.utils.HtmlUtilities;

/**
 * A simple {@link Fragment} subclass.
 */
public class C2Lesson3Fragment extends Fragment {

    WebView wvC2Lesson3;

    public C2Lesson3Fragment() {
        // Required empty public constructor
    }


    public static C2Lesson3Fragment newInstance() {

        Bundle args = new Bundle();

        C2Lesson3Fragment fragment = new C2Lesson3Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_c2_lesson3, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String content = getString(R.string.c2_lesson3_intro);

        wvC2Lesson3 = (WebView) view.findViewById(R.id.wvC2Lesson3);
        wvC2Lesson3.loadData(HtmlUtilities.toHtmlFormat(content),
                HtmlUtilities.CONTENT_TYPE,
                null);
    }
}
