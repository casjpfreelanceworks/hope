package com.bupc.hope.module.lesson.chapterTwo;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.bupc.hope.R;
import com.bupc.hope.core.adapter.chapter.ImageAdapter2;
import com.bupc.hope.core.adapter.chapter.ImageAdapter3;
import com.bupc.hope.core.model.Image;
import com.bupc.hope.core.utils.HtmlUtilities;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class C2Lesson1Fragment extends Fragment {

    RecyclerView rvC2Lesson1;
    ImageAdapter3 imageAdapter2;

    WebView wvC2Lesson1Intro;

    public C2Lesson1Fragment() {
        // Required empty public constructor
    }

    public static C2Lesson1Fragment newInstance() {

        Bundle args = new Bundle();

        C2Lesson1Fragment fragment = new C2Lesson1Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageAdapter2 = new ImageAdapter3(getActivity(), images());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_c2_lesson1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String content = getString(R.string.c2_lesson1_intro);

        wvC2Lesson1Intro = (WebView) view.findViewById(R.id.wvC2Lesson1Intro);
        wvC2Lesson1Intro.loadData(HtmlUtilities.toHtmlFormat(content),
                HtmlUtilities.CONTENT_TYPE,
                null);

        rvC2Lesson1 = (RecyclerView) view.findViewById(R.id.rvC2Lesson1);
        rvC2Lesson1.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvC2Lesson1.setAdapter(imageAdapter2);
    }

    private List<Image> images() {
        List<Image> images = new ArrayList<>();
        String descriptions[] = getActivity().getResources().getStringArray(R.array.c2_lesson1_list);
        int img[] = {
                R.drawable.c2_direction_of_forces,
                R.drawable.c2_1dot1,
                R.drawable.c2_1dot2,
                R.drawable.c2_strees_fracture
        };

        int drawale = 0;
        for (String description : descriptions) {
             images.add(new Image.Builder()
                                .description(description)
                                .src(img[drawale])
                                .build());
            drawale++;

        }

        return images;
    }
}
