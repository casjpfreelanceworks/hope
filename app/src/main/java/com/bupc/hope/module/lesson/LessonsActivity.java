package com.bupc.hope.module.lesson;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.bupc.hope.R;
import com.bupc.hope.core.model.LessonContent;
import com.bupc.hope.module.lesson.chapterFour.C4Lesson1Fragment;
import com.bupc.hope.module.lesson.chapterFour.C4Lesson2Fragment;
import com.bupc.hope.module.lesson.chapterOne.C1Lesson1Fragment;
import com.bupc.hope.module.lesson.chapterOne.C1Lesson2Fragment;
import com.bupc.hope.module.lesson.chapterThree.C3LessonFragment;
import com.bupc.hope.module.lesson.chapterTwo.C2Lesson1Fragment;
import com.bupc.hope.module.lesson.chapterTwo.C2Lesson2Fragment;
import com.bupc.hope.module.lesson.chapterTwo.C2Lesson3Fragment;

public class LessonsActivity extends AppCompatActivity {

    public static final String CONTENT = "content";

    LessonContent lessonContent;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons);
        initViews();
    }


    private void initViews() {
        setupToolbar();

        Intent content = getIntent();

        if (content != null) {
            lessonContent = (LessonContent) content.getSerializableExtra(CONTENT);
            getSupportActionBar().setTitle(lessonContent.getLessonTitle());
            setupContent(lessonContent);

        } else {
            finish();
        }
    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setupContent(LessonContent content) {

        Fragment fragment = null;

        switch (content.getChapter()) {
            default:
            case 1:
                fragment = chapterOne(content.getLesson());
                break;
            case 2:
                fragment = chapterTwo(content.getLesson());
                break;
            case 3:
                fragment = chapterThree(content.getLesson());
                break;
            case 4:
                fragment = chapterFour(content.getLesson());
                break;
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.flLessonContent, fragment)
                .commit();
    }

    /**
     * for Exercise For Fitness
     * @param lessonIndex
     * @return
     */
    private Fragment chapterOne(int lessonIndex) {
        Fragment fragment = null;
        switch (lessonIndex) {
            default:
            case 0:
                fragment = C1Lesson1Fragment.newInstance();
                break;
            case 1:
                fragment = C1Lesson2Fragment.newInstance();
                break;
        }

        return fragment;
    }

    /**
     * For Sports
     * @param lessonIndex
     * @return
     */
    private Fragment chapterTwo(int lessonIndex) {
        Fragment fragment = null;
        switch (lessonIndex) {
            default:
            case 0:
                fragment = C2Lesson1Fragment.newInstance();
                break;
            case 1:
                fragment = C2Lesson2Fragment.newInstance();
                break;
            case 2:
                fragment = C2Lesson3Fragment.newInstance();
        }

        return fragment;
    }

    /**
     * For Dance
     * @param lessonIndex
     * @return
     */
    private Fragment chapterThree(int lessonIndex) {
        Fragment fragment = null;
        switch (lessonIndex) {
            default:
            case 0:
                fragment = C3LessonFragment.newInstance(lessonIndex);
                break;
            case 1:
                fragment = C3LessonFragment.newInstance(lessonIndex);
                break;
            case 2:
                fragment = C3LessonFragment.newInstance(lessonIndex);
                break;
            case 3:
                fragment = C3LessonFragment.newInstance(lessonIndex);
                break;
            case 4:
                fragment = C3LessonFragment.newInstance(lessonIndex);
                break;
            case 5:
                fragment = C3LessonFragment.newInstance(lessonIndex);
                break;
        }

        return fragment;
    }

    /**
     * For Recreational Activities
     * @param lessonIndex
     * @return
     */
    private Fragment chapterFour(int lessonIndex) {
        Fragment fragment = null;
        switch (lessonIndex) {
            default:
            case 0:
                fragment = C4Lesson1Fragment.newInstance();
                break;
            case 1:
                fragment = C4Lesson2Fragment.newInstance();
                break;
        }

        return fragment;
    }
}
