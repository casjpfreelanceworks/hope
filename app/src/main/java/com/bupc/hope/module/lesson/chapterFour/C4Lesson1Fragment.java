package com.bupc.hope.module.lesson.chapterFour;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.bupc.hope.R;
import com.bupc.hope.core.adapter.chapter.ImageAdapter2;
import com.bupc.hope.core.adapter.chapter.ImageAdapter3;
import com.bupc.hope.core.model.Image;
import com.bupc.hope.core.utils.HtmlUtilities;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class C4Lesson1Fragment extends Fragment {

    RecyclerView rvC4Lesson1;
    ImageAdapter2 imageAdapter2;

    WebView wvC4L1;

    public C4Lesson1Fragment() {
        // Required empty public constructor
    }


    public static C4Lesson1Fragment newInstance() {

        Bundle args = new Bundle();

        C4Lesson1Fragment fragment = new C4Lesson1Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageAdapter2 = new ImageAdapter2(getActivity(), images());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_c4_lesson1, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String content = getString(R.string.c4_lesson1_intro);
        wvC4L1 = (WebView) view.findViewById(R.id.wvC4L1);
        wvC4L1.loadData(HtmlUtilities.toHtmlFormat(content),
                HtmlUtilities.CONTENT_TYPE,
                null);


        rvC4Lesson1 = (RecyclerView) view.findViewById(R.id.rvC4Lesson1);
        rvC4Lesson1.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvC4Lesson1.setAdapter(imageAdapter2);

    }

    private List<Image> images() {
        List<Image> images = new ArrayList<>();
        String contents[] = getActivity().getResources().getStringArray(R.array.c4_lesson1_list);
        int imageResource[] = {
                R.drawable.c4_l1_1,
                R.drawable.c4_l1_2,
                R.drawable.c4_l1_3,
                R.drawable.c4_l1_4,
                R.drawable.c4_l1_5,
                R.drawable.c4_l1_6
        };

        int drawableIndex = 0;
        for (String content : contents) {
            images.add(new Image.Builder()
                            .description(content)
                            .src(imageResource[drawableIndex])
                            .build());
            drawableIndex++;
        }

        return images;
    }
}
