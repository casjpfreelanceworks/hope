package com.bupc.hope.module.chapter.content;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

import com.bupc.hope.R;
import com.bupc.hope.core.utils.HtmlUtilities;
import com.bupc.hope.core.utils.TextUtilities;
import com.bupc.hope.module.chapter.LessonFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class HopeOneFragment extends Fragment implements View.OnClickListener {

    public static final String CHAPTER_INDEX = "chapter_index";
    public static final String CHAPTER_TITLE = "chapterTitle";

    int chapterIndex;
    String chapterTitle;

    WebView tvChapterOneContent;
    Button btnStartHopeLessonOne;

    public HopeOneFragment() {
        // Required empty public constructor
    }

    public static HopeOneFragment newInstance(int chapterIndex, String chapterTitle) {
        
        Bundle args = new Bundle();
        args.putInt(CHAPTER_INDEX, chapterIndex);
        args.putString(CHAPTER_TITLE, chapterTitle);
        
        HopeOneFragment fragment = new HopeOneFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chapterIndex = getArguments().getInt(CHAPTER_INDEX, 1);
        chapterTitle = getArguments().getString(CHAPTER_TITLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hope_one, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnStartHopeLessonOne = (Button) view.findViewById(R.id.btnStartHopeLessonOne);
        btnStartHopeLessonOne.setOnClickListener(this);

        String content = getResources().getString(R.string.hope1_content);

        Log.d(HopeOneFragment.class.getSimpleName(), content);

        tvChapterOneContent = (WebView) view.findViewById(R.id.tvChapterOneContent);
        tvChapterOneContent.loadData(HtmlUtilities.toHtmlFormat(content), HtmlUtilities.CONTENT_TYPE, null);
    }

    @Override
    public void onClick(View view) {
        getFragmentManager().beginTransaction()
                .replace(R.id.flLessonContainer, LessonFragment.newInstance(chapterIndex, chapterTitle), null)
                .commit();
    }
}
