package com.bupc.hope.module.lesson.chapterOne;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.bupc.hope.R;
import com.bupc.hope.core.adapter.chapter.ImageAdapter2;
import com.bupc.hope.core.adapter.chapter.ImageAdapter3;
import com.bupc.hope.core.model.Image;
import com.bupc.hope.core.utils.HtmlUtilities;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class C1Lesson2Fragment extends Fragment {

    private RecyclerView rvC1Lesson2;

    private ImageAdapter3 imageAdapter2;

    private WebView wvC1Lesson2Intro;

    public C1Lesson2Fragment() {
        // Required empty public constructor
    }

    public static C1Lesson2Fragment newInstance() {

        Bundle args = new Bundle();

        C1Lesson2Fragment fragment = new C1Lesson2Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageAdapter2 = new ImageAdapter3(getActivity(), getImagesResources());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_c1_lesson2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view) {
        String content = getString(R.string.c1_lesson2_intro);
        wvC1Lesson2Intro = (WebView) view.findViewById(R.id.wvC1Lesson2Intro);
        wvC1Lesson2Intro.loadData(HtmlUtilities.toHtmlFormat(content),
                HtmlUtilities.CONTENT_TYPE,
                null);

        rvC1Lesson2 = (RecyclerView) view.findViewById(R.id.rvC1Lesson2);
        rvC1Lesson2.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvC1Lesson2.setAdapter(imageAdapter2);
    }

    private List<Image> getImagesResources() {
        List<Image> images = new ArrayList<>();
        String descriptions[] = getActivity().getResources().getStringArray(R.array.c1_lesson2_list);
        int imagesResource[] = {
                R.drawable.c1_running,
                R.drawable.c1_weightlift,
                R.drawable.c1_stretching
        };

        int drawable = 0;
        for (String description : descriptions) {
            images.add(new Image.Builder()
                    .description(description)
                    .src(imagesResource[drawable]).build());
            drawable++;
        }

        return images;
    }
}
