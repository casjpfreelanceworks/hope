package com.bupc.hope.module.lesson.chapterTwo;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.bupc.hope.R;
import com.bupc.hope.core.utils.HtmlUtilities;

/**
 * A simple {@link Fragment} subclass.
 */
public class C2Lesson2Fragment extends Fragment {

    WebView wvC2Lesson2Intro;
    WebView wvC2Lesson2Intro2;

    public C2Lesson2Fragment() {
        // Required empty public constructor
    }

    public static C2Lesson2Fragment newInstance() {

        Bundle args = new Bundle();

        C2Lesson2Fragment fragment = new C2Lesson2Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_c2_lesson2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String content = getString(R.string.c2_lesson2_intro);
        String content2 = getString(R.string.c2_lesson2_intro2);

        wvC2Lesson2Intro = (WebView) view.findViewById(R.id.wvC2Lesson2Intro);
        wvC2Lesson2Intro2 = (WebView) view.findViewById(R.id.wvC2Lesson2Intro2);

        wvC2Lesson2Intro.loadData(HtmlUtilities.toHtmlFormat(content),
                HtmlUtilities.CONTENT_TYPE,
                null);

        wvC2Lesson2Intro2.loadData(HtmlUtilities.toHtmlFormat(content2),
                HtmlUtilities.CONTENT_TYPE,
                null);
    }
}
