package com.bupc.hope.module.chapter;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bupc.hope.R;
import com.bupc.hope.core.adapter.LessonAdapter;
import com.bupc.hope.core.model.Lesson;
import com.bupc.hope.core.model.LessonContent;
import com.bupc.hope.core.utils.HopeDataUtilities;
import com.bupc.hope.module.lesson.LessonsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class LessonFragment extends Fragment implements LessonAdapter.OnItemClickListener {

    public static final String CHAPTER_INDEX = "chapter_index";
    public static final String CHAPTER_TITLE = "chapterTitle";

    LessonAdapter lessonAdapter;
    RecyclerView rvLessons;
    List<Lesson> lessons;

    int chapterIndex;
    String chapterTitle;

    public static LessonFragment newInstance(int chapterIndex, String chapterTitle) {

        Bundle args = new Bundle();
        args.putInt(CHAPTER_INDEX, chapterIndex);
        args.putString(CHAPTER_TITLE, chapterTitle);

        LessonFragment fragment = new LessonFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public LessonFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lessons = new ArrayList<>();
        chapterIndex = getArguments().getInt(CHAPTER_INDEX, 1);
        chapterTitle = getArguments().getString(CHAPTER_TITLE);

        lessons = HopeDataUtilities.getResourceLesson(getActivity(), chapterIndex);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lesson, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initComponents();
    }

    private void initViews(View view) {
        rvLessons = (RecyclerView) view.findViewById(R.id.rvLessons);
    }

    private void initComponents() {
        lessonAdapter = new LessonAdapter(getActivity(), lessons, this);
        rvLessons.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvLessons.setAdapter(lessonAdapter);
    }


    @Override
    public void onItemClick(int position) {
        startLessonActivity(chapterIndex, position, lessons.get(position).getLesson());
    }

    private void startLessonActivity(int chapterIndex, int position, String lessonTitle) {
        LessonContent.Builder builder = new LessonContent.Builder()
                .chapter(chapterIndex)
                .lesson(position)
                .title(chapterTitle);

        Intent intent = new Intent(getActivity(), LessonsActivity.class);
        intent.putExtra(LessonsActivity.CONTENT, builder.build());

        startActivity(intent);
    }
}
