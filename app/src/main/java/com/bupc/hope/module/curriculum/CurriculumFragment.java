package com.bupc.hope.module.curriculum;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bupc.hope.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnPageScrollListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurriculumFragment extends Fragment implements OnPageChangeListener, OnLoadCompleteListener, OnPageScrollListener {


    private final static int REQUEST_CODE = 42;
    public static final int PERMISSION_CODE = 42042;

    private TextView tvDepedTitle;

    private Integer pageNumber = 0;
    int currentPosition = 0;
    private int shortAnimation;

    PDFView pdfCurriculum;

    float prevOffset = 0.0f;

    private static final String PDF = "k212.pdf";

    public CurriculumFragment() {
        // Required empty public constructor
    }


    public static CurriculumFragment newInstance() {
        
        Bundle args = new Bundle();
        
        CurriculumFragment fragment = new CurriculumFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_curriculum, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pdfCurriculum = (PDFView) view.findViewById(R.id.pdfCurriculum);

        pdfCurriculum.fromAsset(PDF)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableDoubletap(true)
                .onPageChange(this)
                .onPageScroll(this)
                .defaultPage(0)
                .enableAnnotationRendering(false)
                .load();

        shortAnimation = getResources().getInteger(android.R.integer.config_shortAnimTime);
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;

    }

    @Override
    public void loadComplete(int nbPages) {

    }

    @Override
    public void onPageScrolled(int page, float positionOffset) {

    }

}
