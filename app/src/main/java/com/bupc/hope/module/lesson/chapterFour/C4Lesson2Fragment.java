package com.bupc.hope.module.lesson.chapterFour;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.bupc.hope.R;
import com.bupc.hope.core.adapter.chapter.ImageAdapter2;
import com.bupc.hope.core.adapter.chapter.ImageAdapter3;
import com.bupc.hope.core.model.Image;
import com.bupc.hope.core.utils.HtmlUtilities;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class C4Lesson2Fragment extends Fragment {


    ImageAdapter3 imageAdapter2;
    RecyclerView rvC4Lesson2;

    WebView wvC4L2;

    public C4Lesson2Fragment() {
        // Required empty public constructor
    }

    public static C4Lesson2Fragment newInstance() {

        Bundle args = new Bundle();

        C4Lesson2Fragment fragment = new C4Lesson2Fragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageAdapter2 = new ImageAdapter3(getActivity(), images());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_c4_lesson2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String content = getString(R.string.c4_lesson2_intro);
        wvC4L2 = (WebView) view.findViewById(R.id.wvC4L2);
        wvC4L2.loadData(HtmlUtilities.toHtmlFormat(content),
                HtmlUtilities.CONTENT_TYPE,
                null);

        rvC4Lesson2 = (RecyclerView) view.findViewById(R.id.rvC4Lesson2);
        rvC4Lesson2.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvC4Lesson2.setAdapter(imageAdapter2);
    }

    private List<Image> images() {
        List<Image> images = new ArrayList<>();
        String contents[] = getActivity().getResources().getStringArray(R.array.c4_lesson2_list);
        int imageResource[] = {
                R.drawable.c4_l2_1,
                R.drawable.c4_l2_2,
                R.drawable.c4_l2_3,
                R.drawable.c4_l2_4,
        };

        int drawableIndex = 0;
        for (String content : contents) {
            images.add(new Image.Builder()
                    .description(content)
                    .src(imageResource[drawableIndex])
                    .build());
            drawableIndex++;
        }

        return images;
    }
}
