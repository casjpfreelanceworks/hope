package com.bupc.hope.module.chapter;

import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bupc.hope.R;
import com.bupc.hope.core.adapter.LessonAdapter;
import com.bupc.hope.core.model.Chapter;
import com.bupc.hope.module.chapter.content.HopeFourFragment;
import com.bupc.hope.module.chapter.content.HopeOneFragment;
import com.bupc.hope.module.chapter.content.HopeThreeFragment;
import com.bupc.hope.module.chapter.content.HopeTwoFragment;

public class ChapterActivity extends AppCompatActivity {

    public static final String EXTRA_TITLE = "chapter";

    LessonAdapter lessonAdapter;

    Toolbar toolbar;
    CollapsingToolbarLayout toolbarLayout;

    TextView tvDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter);
        initViews();
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        final Chapter chapter = (Chapter) intent.getSerializableExtra(EXTRA_TITLE);

       /* tvDescription = (TextView) findViewById(R.id.tvDescription);*/
        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        setupDescription(chapter.getDescription());
        setupCollapsingToolbar(chapter.getTitle());
        loadIvHome(chapter.getDrawableIndex());

        setupLayoutContainer(chapter);
    }

    private void setupLayoutContainer(Chapter chapter) {

        Fragment fragment = getFragment(chapter);

        if (fragment != null) {
            /*getSupportFragmentManager().beginTransaction()
                    .replace(R.id.flLessonContainer, LessonFragment.newInstance(chapter.getDrawableIndex(), chapter.getTitle()), null)
                    .commit();*/
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.flLessonContainer, fragment, null)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.flLessonContainer, HopeOneFragment.newInstance(chapter.getDrawableIndex(), chapter.getTitle() ), null)
                    .commit();
        }

    }

    private Fragment getFragment(Chapter chapter) {
        Fragment fragment = null;
        switch (chapter.getDrawableIndex()) {
            default:
            case 1:
                fragment = HopeOneFragment.newInstance(chapter.getDrawableIndex(), chapter.getTitle());
                break;
            case 2:
                fragment = HopeTwoFragment.newInstance(chapter.getDrawableIndex(), chapter.getTitle());
                break;
            case 3:
                fragment = HopeThreeFragment.newInstance(chapter.getDrawableIndex(), chapter.getTitle());
                break;
            case 4:
                fragment = HopeFourFragment.newInstance(chapter.getDrawableIndex(), chapter.getTitle());
                break;
        }

        return fragment;
    }

    private void setupDescription(String description) {
    }

    private void loadIvHome(int drawableIndex) {
        final ImageView ivHome = (ImageView) findViewById(R.id.ivHope);
        Glide.with(this).load(Chapter.getChapterDrawable(drawableIndex)).centerCrop().into(ivHome);
    }

    private void setupCollapsingToolbar(String title) {
        toolbarLayout.setTitle(title);
    }
}
