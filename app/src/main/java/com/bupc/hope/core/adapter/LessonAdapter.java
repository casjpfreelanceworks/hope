package com.bupc.hope.core.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bupc.hope.R;
import com.bupc.hope.core.model.Lesson;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by John Paul Cas on 11/23/2016.
 */

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.ViewHolder> {


    private LayoutInflater inflater;
    private List<Lesson> lessons;
    private OnItemClickListener clickListener;

    public LessonAdapter(Context context, List<Lesson> lessons, OnItemClickListener clickListener) {
        inflater = LayoutInflater.from(context);
        this.lessons = lessons;
        this.clickListener = clickListener;
    }

    @Override
    public LessonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.row_chapter, parent, false);
        ViewHolder holder = new ViewHolder(row, clickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(LessonAdapter.ViewHolder holder, int position) {
        Lesson chapter = lessons.get(position);
        holder.tvLesson.setText(chapter.getLesson());
        holder.tvShortDescription.setText(chapter.getContent());
    }

    @Override
    public int getItemCount() {
        return lessons.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvLesson;
        private TextView tvShortDescription;
        private OnItemClickListener clickListener;

        public ViewHolder(View view, OnItemClickListener listener) {
            super(view);
            this.clickListener = listener;
            tvLesson = (TextView) view.findViewById(R.id.tvLesson);
            tvShortDescription = (TextView) view.findViewById(R.id.tvShortDescription);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(int position);
    }


}
