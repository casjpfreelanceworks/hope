package com.bupc.hope.core.model;

import java.io.Serializable;

/**
 * Created by John Paul Cas on 12/4/2016.
 */

public class LessonContent implements Serializable {

    private int chapter;
    private int lesson;
    private String lessonTitle;

    public LessonContent(Builder builder) {
        setChapter(builder.chapter);
        setLesson(builder.lesson);
        setLessonTitle(builder.title);
    }

    public int getChapter() {
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }

    public int getLesson() {
        return lesson;
    }

    public void setLesson(int lesson) {
        this.lesson = lesson;
    }

    public String getLessonTitle() {
        return lessonTitle;
    }

    public void setLessonTitle(String lessonTitle) {
        this.lessonTitle = lessonTitle;
    }

    public static class Builder {
        private int chapter;
        private int lesson;
        private String title;

        public Builder chapter(int chapter) {
            this.chapter = chapter;
            return this;
        }

        public Builder lesson(int lesson) {
            this.lesson = lesson;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public LessonContent build() {
            return new LessonContent(this);
        }
    }
}
