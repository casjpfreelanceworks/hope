package com.bupc.hope.core.enumz;

/**
 * Created by John Paul Cas on 11/20/2016.
 */

public enum Swipe {
    UP,
    DOWN
}
