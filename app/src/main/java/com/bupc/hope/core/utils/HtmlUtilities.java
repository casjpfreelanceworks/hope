package com.bupc.hope.core.utils;

/**
 * Created by John Paul Cas on 2/8/2017.
 */

public class HtmlUtilities {
    public static final String CONTENT_TYPE = "text/html; charset=utf-8";
    public static final String ENCODING = "utf-8";

    public static final String toHtmlFormat(String content) {
        return new StringBuilder()
                .append("<html>")
                .append("<body style=\"text-align:justify;\">")
                .append(content)
                .append("</body>")
                .append("</html>")
                .toString();
    }
}
