package com.bupc.hope.core.model;

import com.bupc.hope.R;

import java.io.Serializable;

/**
 * Created by John Paul Cas on 11/16/2016.
 */

public class Chapter implements Serializable {

    private String title;
    private String description;
    private int drawableIndex;

    public Chapter(Builder builder) {
        setTitle(builder.title);
        setDescription(builder.description);
        setDrawableIndex(builder.drawableIndex);
    }

    public static int getChapterDrawable(int index) {

        switch (index) {
            default:
            case 1:
                return R.drawable.chapter1;
            case 2:
                return R.drawable.chapter2;
            case 3:
                return R.drawable.chapter3;
            case 4:
                return R.drawable.chapter4;
        }

    }

    /**
     *
     * @return
     * The Title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The Description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public int getDrawableIndex() {
        return drawableIndex;
    }

    public void setDrawableIndex(int drawableIndex) {
        this.drawableIndex = drawableIndex;
    }

    public static class Builder {
        private String title;
        private String description;
        private int drawableIndex;

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder drawableIndex(int drawableIndex) {
            this.drawableIndex = drawableIndex;
            return this;
        }

        public Chapter build() {
            return new Chapter(this);
        }
    }
}
