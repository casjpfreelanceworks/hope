package com.bupc.hope.core.model;

import android.support.annotation.ColorRes;

import java.io.Serializable;

/**
 * Created by John Paul Cas on 11/20/2016.
 */

public class Lesson implements Serializable {


    private String lesson;
    private String content;
    private String contentFileDir;
    private int layoutResource;
    int color;

    public Lesson(Builder builder) {
        setLesson(builder.lesson);
        setContent(builder.content);
        setContentFileDir(builder.contentFileDir);
        setColor(builder.color);
    }

    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentFileDir() {
        return contentFileDir;
    }

    public void setContentFileDir(String contentFileDir) {
        this.contentFileDir = contentFileDir;
    }

    public int getColor() {
        return color;
    }

    public void setColor(@ColorRes int color) {
        this.color = color;
    }

    public int getLayoutResource() {
        return layoutResource;
    }

    public void setLayoutResource(int layoutResource) {
        this.layoutResource = layoutResource;
    }

    public static class Builder {

        private String lesson;
        private String content;
        private String contentFileDir;
        private int color;
        private int layoutResource;

        public Builder lesson(String lesson) {
            this.lesson = lesson;
            return this;
        }

        public Builder content(String content) {
            this.content = content;
            return this;
        }

        public Builder fileDir(String contentFileDir) {
            this.contentFileDir = contentFileDir;
            return this;
        }

        public Builder color(@ColorRes int color) {
            this.color = color;
            return this;
        }

        public Builder layoutResource(int layoutResource) {
            this.layoutResource = layoutResource;
            return this;
        }

        public Lesson build() {
            return new Lesson(this);
        }
    }
}
