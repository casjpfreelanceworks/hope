package com.bupc.hope.core.enumz;

/**
 * Created by John Paul Cas on 11/19/2016.
 */

public enum Page {
    HOME,
    CURRICULUM,
    ABOUT
}
