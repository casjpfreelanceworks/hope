package com.bupc.hope.core.adapter.chapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bupc.hope.R;
import com.bupc.hope.core.model.Image;
import com.bupc.hope.core.utils.HtmlUtilities;

import java.util.List;

/**
 * Created by John Paul Cas on 2/9/2017.
 */

public class ImageAdapter3 extends RecyclerView.Adapter<ImageAdapter3.ViewHolder> {

    List<Image> images;
    Context context;

    LayoutInflater inflater;

    public ImageAdapter3(Context context, List<Image> images) {
        this.context = context;
        this.images = images;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.row_simple_image_webview_layout, parent, false);
        ViewHolder holder = new ViewHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Image image = images.get(position);

        holder.wvTitle.loadData(HtmlUtilities.toHtmlFormat(image.getDescription()),
                HtmlUtilities.CONTENT_TYPE,
                null);
        holder.wvTitle.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        Glide.with(context)
                .load(image.getSource())
                .crossFade()
                .into(holder.ivImageResource);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private WebView wvTitle;
        private ImageView ivImageResource;

        public ViewHolder(View row) {
            super(row);
            wvTitle = (WebView) row.findViewById(R.id.wvTitle);
            ivImageResource = (ImageView) row.findViewById(R.id.ivImageResource2);


        }
    }
}
