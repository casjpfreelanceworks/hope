package com.bupc.hope.core.utils;

import android.content.Context;

import com.bupc.hope.R;
import com.bupc.hope.core.model.Chapter;
import com.bupc.hope.core.model.Lesson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by John Paul Cas on 11/16/2016.
 */

public class HopeDataUtilities {

    public static List<Chapter> getResourceData(Context context) {
        List<Chapter> chapters = new ArrayList<>();

        String[] contents = context.getResources().getStringArray(R.array.chapter);
        String lorem = context.getString(R.string.lorem);
        int index = 1;

        for (String content:
             contents) {
            Chapter.Builder builder = new Chapter.Builder()
                    .title(content)
                    .description(lorem)
                    .drawableIndex(index);

            chapters.add(builder.build());
            index++;
        }

        return chapters;
    }

    public static List<Lesson> getResourceLesson(Context context, int chapterIndex) {
        List<Lesson> lessons = new ArrayList<>();

        String []resouce = new String[10];
        String []shortDescription = new String[10];

        switch (chapterIndex) {
            default:
            case 1:
                resouce = context.getResources().getStringArray(R.array.hope1_lesson);
                shortDescription = context.getResources().getStringArray(R.array.hope1_lesson_shortdescription);
                break;
            case 2:
                resouce = context.getResources().getStringArray(R.array.hope2_lesson);
                shortDescription = context.getResources().getStringArray(R.array.hope2_lesson_shortdescription);
                break;
            case 3:
                resouce = context.getResources().getStringArray(R.array.hope3_lesson);
                shortDescription = context.getResources().getStringArray(R.array.hope3_lesson_shortdescription);
                break;
            case 4:
                resouce = context.getResources().getStringArray(R.array.hope4_lesson);
                shortDescription = context.getResources().getStringArray(R.array.hope4_lesson_shortdescription);
                break;
        }

        String lorem = context.getString(R.string.lorem);

        int index = 0;
        for (String lessonString : resouce) {
            Lesson.Builder lessonBuilder = new Lesson.Builder()
                    .lesson(lessonString)
                    .content(shortDescription[index]);

            lessons.add(lessonBuilder.build());
            index++;
        }

        return lessons;
    }

}
