package com.bupc.hope.core.adapter.chapter.one;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bupc.hope.R;
import com.bupc.hope.core.model.Image;

import java.util.List;

/**
 * Created by John Paul Cas on 12/4/2016.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    private List<Image> images;
    private Context context;

    private LayoutInflater inflater;


    public ImageAdapter(Context context, List<Image> images) {
        this.images = images;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.row_simple_img_layout, parent, false);
        ViewHolder holder = new ViewHolder(row);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Image image = images.get(position);
        holder.tvImageTitle.setText(image.getTitle());
        Glide.with(context)
                .load(image.getSource())
                .crossFade()
                .into(holder.ivImageResource);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvImageTitle;
        private ImageView ivImageResource;

        public ViewHolder(View row) {
            super(row);
            tvImageTitle = (TextView) row.findViewById(R.id.tvImageTitle);
            ivImageResource = (ImageView) row.findViewById(R.id.ivImageResource);
        }
    }
}
