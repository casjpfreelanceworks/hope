package com.bupc.hope.core.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bupc.hope.R;
import com.bupc.hope.core.model.Chapter;

import java.util.List;

/**
 * Created by John Paul Cas on 11/16/2016.
 */

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.ViewHolder> {
    private OnItemClickListener onItemClick;

    private LayoutInflater inflater;
    private List<Chapter> chapters;

    public ChapterAdapter(Context context, List<Chapter> chapters, OnItemClickListener onItemClick) {
        inflater = LayoutInflater.from(context);
        this.chapters = chapters;

        this.onItemClick = onItemClick;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.row_hope, parent, false);
        ViewHolder holder = new ViewHolder(row, onItemClick);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Chapter chapter = chapters.get(position);
        holder.tvTitle.setText(chapter.getTitle());

        Glide.with(holder.ivThumbnail.getContext())
                .load(Chapter.getChapterDrawable(chapter.getDrawableIndex()))
                .fitCenter()
                .into(holder.ivThumbnail);
    }

    @Override
    public int getItemCount() {
        return chapters.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvTitle;
        private ImageView ivThumbnail;
        private OnItemClickListener clickListener;

        public ViewHolder(View view, OnItemClickListener clickListener) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            ivThumbnail = (ImageView) view.findViewById(R.id.ivThumbnail);

            this.clickListener = clickListener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
