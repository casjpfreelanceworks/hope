package com.bupc.hope.core.model;

/**
 * Created by John Paul Cas on 12/4/2016.
 */

public class Image {

    private String title;
    private String caption;
    private String description;
    private int source;


    public Image(Builder builder) {
        setTitle(builder.title);
        setCaption(builder.caption);
        setDescription(builder.description);
        setSource(builder.resource);
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public static class Builder {
        private String title;
        private String caption;
        private String description;
        private int resource;

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder caption(String caption) {
            this.caption = caption;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder src(int resource) {
            this.resource = resource;
            return this;
        }

        public Image build() {
            return new Image(this);
        }
    }
}
